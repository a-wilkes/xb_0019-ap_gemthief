<pre>
 ██████╗ ███████╗███╗   ███╗    ████████╗██╗  ██╗██╗███████╗███████╗
██╔════╝ ██╔════╝████╗ ████║    ╚══██╔══╝██║  ██║██║██╔════╝██╔════╝
██║  ███╗█████╗  ██╔████╔██║       ██║   ███████║██║█████╗  █████╗  
██║   ██║██╔══╝  ██║╚██╔╝██║       ██║   ██╔══██║██║██╔══╝  ██╔══╝  
╚██████╔╝███████╗██║ ╚═╝ ██║       ██║   ██║  ██║██║███████╗██║     
 ╚═════╝ ╚══════╝╚═╝     ╚═╝       ╚═╝   ╚═╝  ╚═╝╚═╝╚══════╝╚═╝
</pre>

---

To open and the run project follow these hopefully, but unlikely to be, thorough instructions:

+ Open IntelliJ
+ *File* &xrarr; *New* &xrarr; *Project from existing sources...*
+ Select root folder of this project: **gemthief_aws241**
+ Select **Import project from external model** and **Gradle**

The above (should) ensure that `gradlew run` works. If not, follow the steps below:

+ *File* &xrarr; *Project Structure...* &xrarr; *Project* &xrarr; *Project SDK*
+ Select **Java 1.8** and *Project Language Level* as **8**
+ *Run* &xrarr; *Edit configurations...* &xrarr; *+* &xrarr; *Application*
+ *Use classpath of module:* **gemthief.main**
+ *Main class:* **game.base.GameBase**
+ *Name:* **Gem Thief**
+ *Run* &xrarr; *Run 'Gem Thief'*

---

### How to Play

Collect all the gems before the time runs out.

Run around with the `arrow keys`, jump with `space` and crouch with `ctrl`.

---

### Description

The real aim of the game is to fight an overly sensitive control scheme, survive travelling too quickly on the diagonal, and avoid encountering questionable collision detection and resolution (there's a bug where if you repeatedly jump off the world and restart, eventually you get stuck  falling out of the world on every re-spawn. I have no idea why this happens).

The main things I'd like to improve if I had more time would be finding a way to add in shadows (depth perception is a bit of an issue at the moment) and making a decent level generator - currently it's almost entirely "random", which works fine as a proof of concept, but doesn't exactly create an enticing world.

#### Reused Code
In terms of code that was written, only the Colour class from the mandatory assignments remains. Since I wrote Missile Command first this project reuses a lot of code from there - the shared code is mainly explained in the Missile Command ReadMe.

Due to the reuse, there is an awkward mix of PVectors and Coordinates inside of Renderer. The advantage of creating Coordinates for Missile Command was that I only needed the very basics and could write it to be immutable at the same time (I'm doing my best to embrace the functional paradigm). Since Gem Thief is in 3D, however, using Processing's PVector had more advantages, thus the blend.

#### New Code
The new code for this project is almost exclusively found inside the game.logic package and the Renderer class in the engine package (GameBase also had some minor changes - mostly just code removal).

The most difficult aspects of the project were writing a collision detection and resolution scheme that worked properly and designing the player as a state machine.

##### Collision Detection
The collision detection wasn't so much difficult as it was fiddly, though it's worth observing that it is very rudimentary - since find out what wide and narrow collision detection were seemed overkill, the game logic just checks if any object in the whole world is colliding with the player. This is obviously wildly inefficient, so I chose to use AABB detection as it's both simple and fast. Aside from the aforementioned "Suicide Bug", it seems to work sufficiently.

##### Player State Machine
The player state machine was also a fair amount of effort to design and write - since I've never tried functional programming outside of this course, I wanted to get into it as best I could. It occurred to me that, since every function call on Actor would return a new object (functional immutability) I could also return a new subclass instead. This avoided the need to write error-prone control flows since I could just rewrite methods as needed (e.g. the DefaultState returns a JumpState when jump() is called but JumpState will only return itself if jump() is called again - i.e. you can't jump whilst jumping). I'm really happy with how this turned out as I think it ended up feeling quite nice to program with as there was a lot less thinking involved once it worked - since everything was immutable, I knew the player could never end up in an invalid state, and that it couldn't do anything that I hadn't explicitly written (e.g. no crouching whilst jumping).

---

Sounds: taken from http://freesound.org under [Creative Commons](https://creativecommons.org/publicdomain/zero/1.0/)

Libraries: [Minim Audio Library](http://code.compartmental.net/tools/minim/) under [GNU Lesser GPL](https://www.gnu.org/licenses/lgpl-3.0.en.html)