package engine

import processing.core.PVector

class Coordinate(val x: Float, val y: Float) {

  def this(v: PVector) =
    this(v.x, v.y)
}
