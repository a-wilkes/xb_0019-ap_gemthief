package engine

sealed trait Direction
case object LEFT extends Direction
case object RIGHT extends Direction
case object FORWARD extends Direction
case object BACK extends Direction