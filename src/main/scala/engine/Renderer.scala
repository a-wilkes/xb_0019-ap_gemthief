package engine

import engine.graphics.Colour
import engine.Renderer._
import game.base.GameBase.{HeightInPixels, WidthInPixels}
import game.logic.GameLogic
import game.logic.objects.{Actor, Gem, Platform, Renderables}
import processing.core.{PApplet, PConstants, PVector}


class Renderer extends PApplet {
  // deltaTime represents how far into the next frame we are when render is called.
  // This allows predictive interpolation to render objects in (probably) the right place mid-update
  // To avoid artifacts when everything should be stopped (game over) the interpolation is set to 0 in drawMenu
  protected def render(gl: GameLogic): Unit = {
    setBackgroundColour(Colour.Black)

    if (gl.gameIsOver) drawGameOver(gl)
    else {
      drawWorld(gl.player, gl.render())
      drawGUI(gl.score, gl.maxScore, gl.timeRemaining)
    }
  }

  private def drawWorld(player: Actor, r: Renderables): Unit = {
    drawGameWorldLights()
    updateGameCamera(player)

    r.characters.foreach(drawCharacter)
    r.gems.foreach(drawGem)
    r.platforms.foreach(drawPlatform)
  }

  private def drawGameOver(gl: GameLogic): Unit = {
    if (gl.gameIsWon) drawGameWon(gl.score, gl.maxScore, gl.timeRemaining)
    else if (gl.playerIsDead) drawGameOverDied(gl.score, gl.maxScore, gl.timeRemaining)
    else drawGameOverTime(gl.score, gl.maxScore)
  }

  private def drawGUI(score: Int, maxScore: Int, time: Int): Unit = {
    lights()

    val scoreText: String = "Score: " + score.toString + "/" + maxScore.toString
    drawText(scoreText, new Coordinate(scoreText.length * 9, 20), 30, Colour.White)

    val timeText: String = "Time Remaining: " + (time / 100).toString
    drawText(timeText, new Coordinate(WidthInPixels - (timeText.length * 9), 20), 30, Colour.White)

    noLights()
  }


  private def drawGameWorldLights(): Unit = {
    directionalLight(255, 255, 255, 2, 1, 0)
  }


  private def updateGameCamera(player: Actor): Unit =
    camera(player.position.x, player.position.y + CameraYDistance, player.position.z + CameraZDistance,
      player.position.x, player.position.y, player.position.z,
      0, 1, -1)


  private def drawGameOverTime(score: Int, maxScore: Int): Unit = {
    lights()

    val scoreText: String = "Score: " + score.toString + "/" + maxScore.toString
    drawText(scoreText, new Coordinate(WidthInPixels / 2, HeightInPixels / 2 - 100), 30, Colour.White)

    drawText("GAME OVER", new Coordinate(WidthInPixels / 2, HeightInPixels / 2), 50, Colour.Red)
    drawText("You ran out of time", new Coordinate(WidthInPixels / 2, HeightInPixels / 2 + 50), 20, Colour.Red)
    drawText("Click mouse to start again", new Coordinate(WidthInPixels / 2, HeightInPixels / 2 + 150), 20, Colour.White)
  }


  private def drawGameWon(score: Int, maxScore: Int, time: Int): Unit = {
    lights()

    val scoreText: String = "Score: " + score.toString + "/" + maxScore.toString
    drawText(scoreText, new Coordinate(WidthInPixels / 2, HeightInPixels / 2 - 100), 30, Colour.White)

    val timeText: String = "Time Remaining: " + (time / 100).toString
    drawText(timeText, new Coordinate(WidthInPixels / 2, HeightInPixels / 2 - 70), 20, Colour.White)

    drawText("YOU WON", new Coordinate(WidthInPixels / 2, HeightInPixels / 2), 50, Colour.Green)
    drawText("Click mouse to start again", new Coordinate(WidthInPixels / 2, HeightInPixels / 2 + 50), 20, Colour.White)
  }


  private def drawGameOverDied(score: Int, maxScore: Int, time: Int): Unit = {
    lights()

    val scoreText: String = "Score: " + score.toString + "/" + maxScore.toString
    drawText(scoreText, new Coordinate(WidthInPixels / 2, HeightInPixels / 2 - 100), 30, Colour.White)

    val timeText: String = "Time Remaining: " + (time / 100).toString
    drawText(timeText, new Coordinate(WidthInPixels / 2, HeightInPixels / 2 - 70), 20, Colour.White)

    drawText("GAME OVER", new Coordinate(WidthInPixels / 2, HeightInPixels / 2), 50, Colour.Red)
    drawText("Click mouse to start again", new Coordinate(WidthInPixels / 2, HeightInPixels / 2 + 50), 20, Colour.White)
  }


  private def drawCharacter(c: Actor): Unit = {
    drawBox(c.position, new PVector(c.width, c.height, c.width))
    drawCharacterFront(c)
  }


  private def drawCharacterFront(c: Actor): Unit = {
    pushStyle()

    setFillColour(Colour.Red)
    c.direction match {
      case engine.FORWARD => drawSphere(new PVector(c.position.x, c.position.y, c.frontEdge),  5)
      case engine.BACK    => drawSphere(new PVector(c.position.x, c.position.y, c.backEdge),   5)
      case engine.LEFT    => drawSphere(new PVector(c.leftEdge,   c.position.y, c.position.z), 5)
      case engine.RIGHT   => drawSphere(new PVector(c.rightEdge,  c.position.y, c.position.z), 5)
    }

    popStyle()
  }


  private def drawPlatform(p: Platform): Unit =
    drawBox(p.centre, new PVector(p.width, p.height, p.depth), p.colour)

  private def drawGem(g: Gem): Unit = { drawSphere(g.centre, g.radius, g.colour) }


  private def drawBox(p: PVector, dimensions: PVector, c: Colour = Colour.White): Unit = {
    pushStyle()
    pushMatrix()

    translate(p.x, p.y, p.z)
    setFillColour(c)
    box(dimensions.x, dimensions.y, dimensions.z)

    popMatrix()
    popStyle()
  }


  private def drawSphere(p: PVector, r: Int, c: Colour = Colour.White): Unit = {
    pushMatrix()

    translate(p.x, p.y, p.z)
    setFillColour(c)
    sphere(r)

    popMatrix()
  }


  private def drawText(s: String, p: Coordinate, size: Float, c: Colour): Unit = {
    pushStyle()
    pushMatrix()
    hint(PConstants.DISABLE_DEPTH_TEST)
    camera()

    textSize(size)
    setFillColour(c)
    text(s, p.x, p.y)

    hint(PConstants.ENABLE_DEPTH_TEST)
    popMatrix()
    popStyle()
  }


  private def setBackgroundColour(c: Colour): Unit    = { background(c.red, c.green, c.blue, c.alpha) }
  private def setFillColour(c: Colour): Unit          = { fill(c.red, c.green, c.blue, c.alpha) }
}



object Renderer {

  private val CameraYDistance: Int = -500
  private val CameraZDistance: Int =  900
}
