package engine

import ddf.minim.{AudioPlayer, AudioSample, Minim}
import game.base.GameBase
import engine.SoundEngine._


class SoundEngine(gameBase: GameBase) {

  val minim = new Minim(gameBase)

  var gameWon: Option[AudioSample]   = None
  var gameLost: Option[AudioSample]  = None
  var gemPickUp: Option[AudioSample] = None


  def setup(): Unit = {
    gameWon   = Option(minim.loadSample(GameWonFilePath))
    gameLost  = Option(minim.loadSample(GameLostFilePath))
    gemPickUp = Option(minim.loadSample(GemPickUpFilePath))
  }

  def playSample(s: Option[AudioSample])(): Unit =
    s match {
      case Some(s) => s.trigger()
      case None =>
    }
}



object SoundEngine {
  val MainFilePath: String      = "res/sound/"
  val GameWonFilePath: String   = MainFilePath + "game-won.mp3"
  val GameLostFilePath: String  = MainFilePath + "game-lost.mp3"
  val GemPickUpFilePath: String = MainFilePath + "gem-ding.wav"
}
