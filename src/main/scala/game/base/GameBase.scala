package game.base

import engine.{Renderer, SoundEngine}
import game.base.GameBase._
import processing.core.{PApplet, PConstants}
import processing.event.KeyEvent
import game.logic.GameLogic



class GameBase extends Renderer {

  val soundMaster: SoundEngine = new SoundEngine(this)
  var gameLogic: GameLogic = new GameLogic(WidthInPixels, HeightInPixels, soundMaster)

  var previousFrameTime: Int = 0
  var lag: Float = 0.0f



  override def draw(): Unit = {
    update()
    render(gameLogic)
  }


  private def startNewGame(): Unit = {
    gameLogic = new GameLogic(WidthInPixels, HeightInPixels, soundMaster)
    previousFrameTime = currentTime()
  }


  // The code inside update ensures that gameLogic.update() will be called on a fixed interval
  // This allows objects to move at constant rates independent of frame rate and rendering
  // The code to do so is largely adapted from Robert Nystrom's website at http://gameprogrammingpatterns.com/game-loop.html
  private def update(): Unit = {
    calculateLag()

    var renderSkips = 0

    while (lag >= MsPerUpdate && renderSkips < MaxRenderSkips) {
      gameLogic.update()
      renderSkips += 1
      lag -= MsPerUpdate
    }
  }


  private def calculateLag(): Unit = {
    val currentFrameTime: Int = currentTime()
    val elapsedFrameTime: Int = currentFrameTime - previousFrameTime

    previousFrameTime = currentFrameTime

    lag += elapsedFrameTime
  }


  override def mousePressed(): Unit =
    if (gameLogic.gameIsOver) startNewGame()


  override def keyPressed(event: KeyEvent): Unit =
    gameLogic.keyPressed(event, mouseX, mouseY)


  override def keyReleased(event: KeyEvent): Unit =
    gameLogic.keyReleased(event, mouseX, mouseY)


  override def settings(): Unit = {
    pixelDensity(displayDensity())
    size(WidthInPixels, HeightInPixels, PConstants.P3D)
  }


  override def setup(): Unit = {
    textFont(createFont("SansSerif", 200))
    text("", 0, 0)

    textAlign(PConstants.CENTER, PConstants.CENTER)
    frameRate(FramesPerSecond)
    noStroke()

    soundMaster.setup()
  }


  def currentTime(): Int = millis()
}



object GameBase {

  val FramesPerSecond: Int = 60
  val MsPerUpdate: Float   = 1000.0f / (FramesPerSecond * 2.0f)
  val MaxRenderSkips: Int  = 20
  val WidthInPixels: Int   = 1600
  val HeightInPixels: Int  = 900

  def main(args:Array[String]): Unit = {
    PApplet.main("game.base.GameBase")
  }

}
