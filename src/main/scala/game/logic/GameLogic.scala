package game.logic

import java.awt.event.KeyEvent._

import engine.{BACK, FORWARD, LEFT, RIGHT, SoundEngine}
import game.logic.GameLogic._
import game.logic.objects.{Actor, Gem, Platform, Renderables}
import processing.core.PVector
import processing.event.KeyEvent


class GameLogic(Width: Int, Height: Int, soundEngine: SoundEngine) {

  var currentLevel: Level = new Level(Width, Width, Height, soundEngine)

  var player: Actor = currentLevel.initialPlayer
  var gems: Vector[Gem] = currentLevel.gems
  var platforms: Vector[Platform] = currentLevel.platforms

  val maxScore: Int = gems.size
  var score: Int    = 0

  var playedGameOverSound: Boolean = false

  var timeRemaining: Int = gems.size * 200

  var leftDown: Boolean    = false
  var rightDown: Boolean   = false
  var backDown: Boolean    = false
  var forwardDown: Boolean = false



  def gameIsOver: Boolean   = { playerIsDead || gameIsWon || timeIsUp }
  def timeIsUp: Boolean     = { timeRemaining <= 0 }
  def playerIsDead: Boolean = { player.position.y > Height * WorldCutOffRatio }
  def gameIsWon: Boolean    = { score == maxScore }


  def update(): Unit = {
    if (gameIsOver) {
      if (playedGameOverSound) return
      else if (gameIsWon) soundEngine.playSample(soundEngine.gameWon)
      else soundEngine.playSample(soundEngine.gameLost)
      playedGameOverSound = true
    }

    updateGameWorld()
  }

  private def updateGameWorld(): Unit = {
    timeRemaining -= 1

    player = player.update(Gravity)
    gems   = gems.map(_.update())

    player = getNewCharacterPosition(player)

    score += gems.size
    gems.filter(g => g.intersects(player.position)).foreach(_.pickedUp())
    gems   = gems.filter(g => !g.intersects(player.position))
    score -= gems.size
  }


  private def getNewCharacterPosition(c: Actor): Actor =
    platforms.find(p => p.intersects(c.topLeftFrontAnchor, c.width, c.height, c.width)) match {
      case Some(p) =>
        if      (c.position.y <= p.topEdge)     c.setPositionUp(p.topEdge)
        else if (c.position.y >= p.bottomEdge)  c.setPositionDown(p.bottomEdge)
        else if (c.frontEdge  <= p.frontEdge)   c.setPositionFront(p.frontEdge)
        else if (c.backEdge   >= p.backEdge)    c.setPositionBack(p.backEdge)
        else if (c.leftEdge   <= p.leftEdge)    c.setPositionLeft(p.leftEdge)
        else if (c.rightEdge  >= p.rightEdge)   c.setPositionRight(p.rightEdge)
        else /* If inside object, pop to top */ c.setPositionUp(p.topEdge)
      case None => c
    }


  def render(): Renderables =
    new Renderables(Vector() :+ player, platforms, gems)


  def keyPressed(event: KeyEvent, x: Float, y: Float): Unit =
    event.getKeyCode match {
      case VK_UP      => upPressed()
      case VK_DOWN    => downPressed()
      case VK_RIGHT   => rightPressed()
      case VK_LEFT    => leftPressed()
      case VK_CONTROL => player = player.crouch()
      case VK_SPACE   => player = player.jump(PlayerJumpVelocity)
      case _ =>
    }

  def upPressed(): Unit = {
    backDown = true
    player   = player.startMoving(player.velocity.x, -PlayerMoveVelocity, FORWARD)
  }

  def downPressed(): Unit = {
    forwardDown = true
    player      = player.startMoving(player.velocity.x, PlayerMoveVelocity, BACK)
  }

  def rightPressed(): Unit = {
    rightDown = true
    player    = player.startMoving(PlayerMoveVelocity, player.velocity.z, RIGHT)
  }

  def leftPressed(): Unit = {
    leftDown = true
    player   = player.startMoving(-PlayerMoveVelocity, player.velocity.z, LEFT)
  }


  def keyReleased(event: KeyEvent, x: Float, y: Float): Unit =
    event.getKeyCode match {
      case VK_UP      => upReleased()
      case VK_DOWN    => downReleased()
      case VK_RIGHT   => rightReleased()
      case VK_LEFT    => leftReleased()
      case VK_CONTROL => player = player.stand()
      case _ =>
    }

  def upReleased(): Unit = {
    backDown = false

    if (forwardDown)
      player = player.startMoving(player.velocity.x, PlayerMoveVelocity, FORWARD)
    else if (!leftDown && !rightDown)
      player = player.stopMoving()
  }

  def downReleased(): Unit = {
    forwardDown = false

    if (backDown)
      player = player.startMoving(player.velocity.x, -PlayerMoveVelocity, BACK)
    else if (!leftDown && !rightDown)
      player = player.stopMoving()
  }

  def rightReleased(): Unit = {
    rightDown = false

    if (leftDown)
      player = player.startMoving(-PlayerMoveVelocity, player.velocity.z, LEFT)
    else if (!forwardDown && !backDown)
      player = player.stopMoving()
  }

  def leftReleased(): Unit = {
    leftDown = false

    if (rightDown)
      player = player.startMoving(PlayerMoveVelocity, player.velocity.z, RIGHT)
    else if (!forwardDown && !backDown)
      player = player.stopMoving()
  }
}



object GameLogic {
  private val Gravity: PVector = new PVector(0.0f, 0.5f)

  private val WorldCutOffRatio: Float = 1.5f

  private val PlayerMoveVelocity: Float =   8.0f
  private val PlayerJumpVelocity: Float = -14.0f
}

