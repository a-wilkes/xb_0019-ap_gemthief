package game.logic

import engine.{Direction, RIGHT, SoundEngine}
import engine.graphics.Colour
import game.logic.Level._
import game.logic.objects.{Actor, DefaultState, Gem, Platform}
import processing.core.PVector

class Level(val worldWidth: Int,
            val worldDepth: Int,
            val worldHeight: Int,
            val soundEngine: SoundEngine) {

  var initialPlayer: Actor = DefaultPlayer

  val ground: Platform = {
    val defaultGroundPosition: PVector = new PVector(worldWidth / 2, worldHeight)
    val width: Int  = worldWidth
    val height: Int = GroundHeight
    val depth: Int  = worldDepth

    new Platform(defaultGroundPosition, width, height, depth, Colour.DarkGreen)
  }

  val platforms: Vector[Platform]   = Vector(
    ground, generatePlatform(), generatePlatform(), generatePlatform(), generatePlatform(), generatePlatform()
  )

  var gems: Vector[Gem] = platforms.flatMap(generateGems)


  private def generatePlatform(): Platform = {
    def getColourWithAlpha(c: Colour, r: Float): Colour = { Colour(c.red, c.green, c.blue, r * 255) }

    val xPosition: Float = worldWidth  * scala.util.Random.nextFloat()
    val yPosition: Float = worldHeight * (PlatformMaxWorldHeightRatio - (PlatformMaxRatioDecrease * scala.util.Random.nextFloat()))
    val zPosition: Float = (worldDepth * scala.util.Random.nextFloat()) - (worldDepth / 2)

    val width: Int  = PlatformMinWidth  + scala.util.Random.nextInt(PlatformMaxWidth)
    val height: Int = PlatformMinHeight + scala.util.Random.nextInt(PlatformMaxHeight)
    val depth: Int  = PlatformMinDepth  + scala.util.Random.nextInt(PlatformMaxDepth)

    new Platform(new PVector(xPosition, yPosition, zPosition), width, height, depth, getColourWithAlpha(Colour.Gray, 0.8f))
  }

  private def generateGems(p: Platform): Vector[Gem] = {
    (for (_ <- 0 to scala.util.Random.nextInt(5)) yield generateGem(p)).toVector
  }

  private def generateGem(p: Platform): Gem = {
    val xPosition: Float = p.centre.x + ((p.width * scala.util.Random.nextFloat()) - (p.width / 2))
    val yPosition: Float = p.centre.y - (p.height / 2) - GemFloatHeight
    val zPosition: Float = p.centre.z + ((p.depth * scala.util.Random.nextFloat()) - (p.depth / 2))
    val startingY: Float = yPosition - (GemFloatHeight * 0.5f * scala.util.Random.nextFloat())

    val centralPosition: PVector  = new PVector(xPosition, yPosition, zPosition)
    val startingPosition: PVector = new PVector(xPosition, startingY, zPosition)

    new Gem(centralPosition, GemRadius, startingPosition, Colour.Red, () => soundEngine.playSample(soundEngine.gemPickUp)())
  }
}

object Level {
  private val PlayerDefaultDirection: Direction = RIGHT
  private val PlayerDefaultPosition: PVector = new PVector(500.0f, 0.0f)
  private val PlayerDefaultVelocity: PVector = new PVector(0.0f, 0.0f)
  private val PlayerDefaultWidth: Int   = 20
  private val PlayerDefaultHeight: Int  = 80

  private val DefaultPlayer: DefaultState = new DefaultState(
    PlayerDefaultPosition,
    PlayerDefaultDirection,
    PlayerDefaultVelocity,
    PlayerDefaultHeight,
    PlayerDefaultHeight,
    PlayerDefaultWidth
  )

  private val GemRadius: Int      = 10
  private val GemFloatHeight: Int = 40

  private val GroundHeight: Int = 100

  private val PlatformMinWidth: Int  = 100
  private val PlatformMinHeight: Int = 5
  private val PlatformMinDepth: Int  = PlatformMinWidth

  private val PlatformMaxWidth: Int  = 1000 - PlatformMinWidth
  private val PlatformMaxHeight: Int = 50 - PlatformMinHeight
  private val PlatformMaxDepth: Int  = PlatformMaxWidth - PlatformMinWidth


  private val PlatformMaxWorldHeightRatio: Float = 0.9f
  private val PlatformMaxRatioDecrease: Float    = 0.2f
}
