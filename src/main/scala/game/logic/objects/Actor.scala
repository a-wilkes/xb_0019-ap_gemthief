package game.logic.objects

import engine.Direction
import processing.core.PVector


abstract class Actor(val position: PVector,
                     val direction: Direction,
                     val velocity: PVector,
                     val defaultHeight: Int,
                     val height: Int,
                     val width: Int) {

  lazy val crouchHeight: Int = 2 * (defaultHeight / 3)

  def update(acceleration: PVector): Actor

  def setPositionUp(edge: Float): Actor   = setPositionVertical(new PVector(position.x, edge - (height / 2), position.z))
  def setPositionDown(edge: Float): Actor = setPositionVertical(new PVector(position.x, edge + (height / 2), position.z))
  protected def setPositionVertical(p: PVector): Actor

  def setPositionLeft(edge: Float): Actor  = setPositionHorizontal(new PVector(edge - (width / 2), position.y, position.z))
  def setPositionRight(edge: Float): Actor = setPositionHorizontal(new PVector(edge + (width / 2), position.y, position.z))
  def setPositionFront(edge: Float): Actor = setPositionHorizontal(new PVector(position.x, position.y, edge - (width / 2)))
  def setPositionBack(edge: Float): Actor  = setPositionHorizontal(new PVector(position.x, position.y, edge + (width / 2)))
  protected def setPositionHorizontal(p: PVector): Actor

  def jump(jumpVelocity: Float): Actor
  def crouch(): Actor
  def stand(): Actor
  def startMoving(xVelocity: Float, zVelocity: Float, d: Direction): Actor
  def stopMoving(): Actor

  final def topLeftFrontAnchor: PVector = new PVector(position.x - (width / 2), position.y - (height / 2), position.z - (width / 2))
  final def topEdge: Float    = topLeftFrontAnchor.y
  final def bottomEdge: Float = topLeftFrontAnchor.y + height
  final def leftEdge: Float   = topLeftFrontAnchor.x
  final def rightEdge: Float  = topLeftFrontAnchor.x + width
  final def frontEdge: Float  = topLeftFrontAnchor.z
  final def backEdge: Float   = topLeftFrontAnchor.z + width
}


class DefaultState(override val position: PVector,
                   override val direction: Direction,
                   override val velocity: PVector,
                   override val defaultHeight: Int,
                   override val height: Int,
                   override val width: Int)
  extends Actor(position, direction, velocity, defaultHeight, height, width) {

  override def update(acceleration: PVector): Actor =
    new DefaultState(position.add(velocity), direction, velocity.add(acceleration), defaultHeight, height, width)

  override protected def setPositionVertical(p: PVector): Actor =
    new DefaultState(p, direction,
      new PVector(velocity.x, 0.0f, velocity.z),
      defaultHeight, height, width
    )

  override protected def setPositionHorizontal(p: PVector): Actor =
    new DefaultState(p, direction,
      new PVector(0.0f, velocity.y, 0.0f),
      defaultHeight, height, width
    )

  override def jump(jumpVelocity: Float): Actor =
    new JumpState(position, direction,
      velocity.add(new PVector(0.0f, jumpVelocity, 0.0f)),
      defaultHeight, crouchHeight, width
    )

  override def crouch(): Actor =
    new DefaultState(position, direction, velocity, defaultHeight, crouchHeight, width)

  override def stand(): Actor =
    new DefaultState(position, direction, velocity, defaultHeight, defaultHeight, width)

  override def startMoving(xVelocity: Float, zVelocity: Float, d: Direction): Actor =
    new RunningState(position, d,
      new PVector(xVelocity, velocity.y, zVelocity),
      defaultHeight, height, width
    )

  override def stopMoving(): Actor =
    new DefaultState(position, direction, new PVector(0.0f, velocity.y, 0.0f), defaultHeight, height, width)
}


class RunningState(override val position: PVector,
                   override val direction: Direction,
                   override val velocity: PVector,
                   override val defaultHeight: Int,
                   override val height: Int,
                   override val width: Int)
  extends Actor(position, direction, velocity, defaultHeight, height, width) {

  override def update(acceleration: PVector): Actor =
    new RunningState(position.add(velocity), direction, velocity.add(acceleration), defaultHeight, height, width)

  override protected def setPositionVertical(p: PVector): Actor =
    new RunningState(p, direction,
      new PVector(velocity.x, 0.0f, velocity.z),
      defaultHeight, height, width
    )

  override protected def setPositionHorizontal(p: PVector): Actor =
    new RunningState(p, direction,
      new PVector(0.0f, velocity.y, 0.0f),
      defaultHeight, height, width
    )

  override def jump(jumpVelocity: Float): Actor =
    new JumpState(position, direction,
      velocity.add(new PVector(velocity.x * 0.5f, jumpVelocity, 0.0f)),
      defaultHeight, crouchHeight, width
    )

  override def crouch(): Actor =
    new RunningState(position, direction, velocity, defaultHeight, crouchHeight, width)

  override def stand(): Actor =
    new RunningState(position, direction, velocity, defaultHeight, defaultHeight, width)

  override def startMoving(xVelocity: Float, zVelocity: Float, d: Direction): Actor =
    new RunningState(position, d,
      new PVector(xVelocity, velocity.y, zVelocity),
      defaultHeight, height, width
    )

  override def stopMoving(): Actor =
    new DefaultState(position, direction, new PVector(0.0f, velocity.y, 0.0f), defaultHeight, height, width)
}


class JumpState(override val position: PVector,
                override val direction: Direction,
                override val velocity: PVector,
                override val defaultHeight: Int,
                override val height: Int,
                override val width: Int)
  extends Actor(position, direction, velocity, defaultHeight, height, width) {

  override def update(acceleration: PVector): Actor =
    new JumpState(position.add(velocity), direction, velocity.add(acceleration), defaultHeight, crouchHeight, width)

  override protected def setPositionVertical(p: PVector): Actor =
    new DefaultState(p, direction,
      new PVector(velocity.x, 0.0f, velocity.z),
      defaultHeight, defaultHeight, width
    )

  override protected def setPositionHorizontal(p: PVector): Actor =
    new DefaultState(p, direction,
      new PVector(0.0f, velocity.y, 0.0f),
      defaultHeight, defaultHeight, width
    )

  override def jump(jumpVelocity: Float): Actor =
    this

  override def crouch(): Actor =
    this

  override def stand(): Actor =
    this

  override def startMoving(xVelocity: Float, zVelocity: Float, d: Direction): Actor =
    new JumpState(position, d,
      new PVector(xVelocity, velocity.y, zVelocity),
      defaultHeight, crouchHeight, width
    )

  override def stopMoving(): Actor =
    new JumpState(position, direction, new PVector(0.0f, velocity.y, 0.0f), defaultHeight, crouchHeight, width)
}


class CrouchState(override val position: PVector,
                  override val direction: Direction,
                  override val velocity: PVector,
                  override val defaultHeight: Int,
                  override val height: Int,
                  override val width: Int)
  extends Actor(position, direction, velocity, defaultHeight, height, width) {

  override def update(acceleration: PVector): Actor =
    new CrouchState(position.add(velocity), direction, velocity.add(acceleration), defaultHeight, height, width)

  override protected def setPositionVertical(p: PVector): Actor =
    new CrouchState(p, direction,
      new PVector(velocity.x, 0.0f, velocity.z),
      defaultHeight, height, width
    )

  override protected def setPositionHorizontal(p: PVector): Actor =
    new CrouchState(p, direction,
      new PVector(0.0f, velocity.y, 0.0f),
      defaultHeight, height, width
    )

  override def jump(jumpVelocity: Float): Actor =
    new JumpState(position, direction,
      velocity.add(new PVector(0.0f, jumpVelocity * 1.2f, 0.0f)),
      defaultHeight, crouchHeight, width
    )

  override def crouch(): Actor =
    this

  override def stand(): Actor =
    new DefaultState(position, direction, velocity, defaultHeight, defaultHeight, width)

  override def startMoving(xVelocity: Float, zVelocity: Float, d: Direction): Actor =
    new CrouchState(position, d,
      new PVector(xVelocity, velocity.y, zVelocity),
      defaultHeight, height, width
    )

  override def stopMoving(): Actor =
    new CrouchState(position, direction, new PVector(0.0f, velocity.y, 0.0f), defaultHeight, height, width)
}
