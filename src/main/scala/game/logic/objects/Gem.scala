package game.logic.objects

import engine.graphics.Colour
import processing.core.PVector
import game.logic.objects.Gem._

class Gem(val origin: PVector,
          val radius: Int,
          override val centre: PVector,
          override val colour: Colour,
          val pickUpSoundCallback: () => Unit,
          val speed: Float = DefaultFloatSpeed)
  extends Object(origin, radius, radius, radius, colour) {

  val topFloatHeight: Float    = origin.y - DefaultFloatRange
  val bottomFloatHeight: Float = origin.y + DefaultFloatRange


  def update(): Gem = {
    val newSpeed: Float = if (centre.y <= topFloatHeight || centre.y >= bottomFloatHeight) -speed else speed
    val newCenter: PVector = centre.add(0.0f, newSpeed, 0.0f)

    new Gem(origin, radius, newCenter, colour, pickUpSoundCallback, newSpeed)
  }


  def pickedUp(): Unit = { pickUpSoundCallback() }


  override def intersects(p: PVector, w: Int = 0, h: Int = 0, d: Int = 0): Boolean =
    p.x   < topLeftFrontAnchor.x + DefaultGrabRange && p.x + w > topLeftFrontAnchor.x - DefaultGrabRange &&
      p.y < topLeftFrontAnchor.y + DefaultGrabRange && p.y + h > topLeftFrontAnchor.y - DefaultGrabRange &&
      p.z < topLeftFrontAnchor.z + DefaultGrabRange && p.z + d > topLeftFrontAnchor.z - DefaultGrabRange
}


object Gem {

  val DefaultFloatRange: Int   = 20
  val DefaultFloatSpeed: Float = 1.0f
  val DefaultGrabRange: Int    = 50
}
