package game.logic.objects

import engine.graphics.Colour
import processing.core.PVector

class Object(val centre: PVector,
             val width: Int,
             val height: Int,
             val depth: Int,
             val colour: Colour) {

  final def topLeftFrontAnchor: PVector = new PVector(centre.x - (width / 2), centre.y - (height / 2), centre.z - (depth / 2))
  final def topEdge: Float    = topLeftFrontAnchor.y
  final def bottomEdge: Float = topLeftFrontAnchor.y + height
  final def leftEdge: Float   = topLeftFrontAnchor.x
  final def rightEdge: Float  = topLeftFrontAnchor.x + width
  final def frontEdge: Float  = topLeftFrontAnchor.z
  final def backEdge: Float   = topLeftFrontAnchor.z + depth

  def intersects(p: PVector, w: Int = 0, h: Int = 0, d: Int = 0): Boolean =
    p.x   < topLeftFrontAnchor.x + width  && p.x + w > topLeftFrontAnchor.x &&
      p.y < topLeftFrontAnchor.y + height && p.y + h > topLeftFrontAnchor.y &&
      p.z < topLeftFrontAnchor.z + depth  && p.z + d > topLeftFrontAnchor.z
}
