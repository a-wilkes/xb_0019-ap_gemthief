package game.logic.objects

import engine.graphics.Colour
import processing.core.PVector

class Platform(override val centre: PVector,
               override val width: Int,
               override val height: Int,
               override val depth: Int,
               override val colour: Colour)
  extends Object(centre, width, height, depth, colour) {}
