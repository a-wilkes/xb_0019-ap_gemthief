package game.logic.objects

class Renderables(val characters: Vector[Actor],
                  val platforms: Vector[Platform],
                  val gems: Vector[Gem]) {}
